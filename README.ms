
# Upload jdk nor jce to nexus:

curl --http1.1 \
    --user "username:password" \
    --upload-file "filename,like something.tar.gz" \
    https://nexus.blackbelt.cloud/repository/devops/oracle-jdk-8/

# After upload, you can download it with this:

curl https://nexus.blackbelt.cloud/repository/devops/oracle-jdk-8/"filename.zip or .tar" -O


