# ORACLE JDK -> MAVEN -> GCLOUD -> KUBECTL -> HELM and docker client #

FROM frolvlad/alpine-glibc:alpine-3.7

ARG MAVEN_VERSION=3.5.3
ARG MAVEN_USER_HOME_DIR="/root"
ARG MAVEN_SHA=b52956373fab1dd4277926507ab189fb797b3bc51a2a267a193c931fffad8408
ARG MAVEN_BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

ENV PATH=/google-cloud-sdk/bin:$PATH \
    JAVA_VERSION=8 JAVA_UPDATE=171 JAVA_HOME="/usr/lib/jvm/default-jvm" \
    MAVEN_HOME=/usr/share/maven MAVEN_CONFIG="$MAVEN_USER_HOME_DIR/.m2" \
    CLOUD_SDK_VERSION=203.0.0 \
    HELM_VERSION=2.7.2 \
    KUBECTL_VERSION=1.10.0

ENV GOOGLE_CLOUD_TAR="google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz" \
    JDK_TAR="jdk-${JAVA_VERSION}u${JAVA_UPDATE}-linux-x64.tar.gz" \
    JCE_ZIP="jce_policy-${JAVA_VERSION}.zip" \
    HELM_TAR="helm-v${HELM_VERSION}-linux-amd64.tar.gz"

ENV JDK_URL="https://nexus.blackbelt.cloud/repository/devops/oracle-jdk-${JAVA_VERSION}/${JDK_TAR}" \
    JCE_URL="https://nexus.blackbelt.cloud/repository/devops/oracle-jdk-${JAVA_VERSION}/${JCE_ZIP}" \
    GOOGLE_CLOUD_SDK_URL="https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/${GOOGLE_CLOUD_TAR}" \
    HELM_URL="https://storage.googleapis.com/kubernetes-helm/${HELM_TAR}" \
    KUBECTL_URL="https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl"

# ORACLE JDK
RUN apk add --no-cache --virtual=build-dependencies wget ca-certificates unzip && \
    cd "/tmp" && \
    wget ${JDK_URL} && \
        tar -xzf "${JDK_TAR}" && \
        mkdir -p "/usr/lib/jvm" && \
        mv "/tmp/jdk1.${JAVA_VERSION}.0_${JAVA_UPDATE}" "/usr/lib/jvm/java-${JAVA_VERSION}-oracle" && \
        ln -s "java-${JAVA_VERSION}-oracle" "$JAVA_HOME" && \
        ln -s "$JAVA_HOME/bin/"* "/usr/bin/" && \
        rm -rf "$JAVA_HOME/"*src.zip && \
    wget ${JCE_URL} && \
        unzip -jo -d "${JAVA_HOME}/jre/lib/security" "${JCE_ZIP}" && \
        rm "${JAVA_HOME}/jre/lib/security/README.txt" && \
    apk del build-dependencies && rm "/tmp/"*

# MAVEN 
RUN apk add --no-cache curl tar bash procps \
  && mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${MAVEN_BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${MAVEN_SHA}  /tmp/apache-maven.tar.gz" | sha256sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

COPY mvn-entrypoint.sh /usr/local/bin/mvn-entrypoint.sh
COPY settings-docker.xml /usr/share/maven/ref/

ENTRYPOINT ["/usr/local/bin/mvn-entrypoint.sh"]
CMD ["mvn"]

# BB BASE IMAGE
RUN apk --no-cache add --update docker curl python py-crcmod bash openssh-client git python python-dev py-pip wget tar && \
    pip install jinja2==2.7.3 jsonpickle==0.9.2 simplejson==3.8.1 sqlalchemy==1.0.9 && \
    curl -O ${GOOGLE_CLOUD_SDK_URL} && \
        tar xzf ${GOOGLE_CLOUD_TAR} && rm ${GOOGLE_CLOUD_TAR} && \
        ln -s /lib /lib64 && \
        gcloud config set core/disable_usage_reporting true && \
        gcloud config set component_manager/disable_update_check true && \
        gcloud config set metrics/environment github_docker_image && \
        gcloud --version && \
    wget ${HELM_URL} && \
        tar xzf ${HELM_TAR} && rm ${HELM_TAR} && \
        mv linux-amd64/helm /usr/local/bin/helm && \
    curl -LO ${KUBECTL_URL} && \
        chmod +x ./kubectl && \
        mv ./kubectl /usr/local/bin/kubectl

VOLUME ["/root/.config"]

